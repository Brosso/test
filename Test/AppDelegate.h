//
//  AppDelegate.h
//  Test
//
//  Created by BingHuan Wu on 9/9/16.
//  Copyright © 2016 Gynoii. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

