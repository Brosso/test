//
//  ViewController.m
//  Test
//
//  Created by BingHuan Wu on 9/9/16.
//  Copyright © 2016 Gynoii. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [textField resignFirstResponder];
    [self performSegueWithIdentifier:@"cheya" sender:textField];
}

@end
